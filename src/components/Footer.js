import React, { Component } from 'react'

export default class Footer extends Component {
    render() {
        return (
           <div>
  <footer className="main-footer">
    <strong>Copyright © 2022 <a href="https:hospitalcare.co.in">Hospitalcare.co.in</a>.</strong>
    All rights reserved.
    <div className="float-right d-none d-sm-inline-block">
      <b>Made with</b>Love@Sherspect
    </div>
  </footer>
</div>

        )
    }
}
