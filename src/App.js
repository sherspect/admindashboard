//  import logo from './logo.svg';
import React, { Component } from "react";
import Signup from "./components/Signup";
import Signin from "./components/Signin";
import Home from "./components/Home";
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

export default class App extends Component {
  render() {
    // const login = localStorage.getItem("isLoggedIn");

    return (
      <Router>
        <Routes>
            <Route path="/" element={<Signup/>}></Route>
            <Route path="/login" element={<Signin/>}></Route>
            <Route path="/home" element={<Home/>}></Route>
        </Routes>
      </Router>
    );
  }
}
